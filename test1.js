
var readline = require('readline');

var TicTacToe = function(readline) {

	/*
	 * Private API
	 * ------------------------------------------------------------------------
	 */

	var _rl = null
		, _boardSize = 3
		, _boardData = {}
		, _playerMarks = ['x', 'o']
		, _players = [];

	_init(readline);


	/**
	 * Initializes the readline interface
	 * @private
	 * @param {Object} Readline module
	 */
	function _init(readline) {
		_rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		});
	}

	/**
	 * Builds the board data object with a 3x3 array and an array of
	 * empty cells
	 * @private
	 */
	function _buildBoard() {
		var data = {
			board: [],
			empty: []
		};

		for (var i = 0; i < _boardSize; i += 1) {
			var row = [];

			for (var k = 0; k < _boardSize; k += 1) {
				row.push(null);
				data.empty.push(i + ' ' + k);
			}

			data.board.push(row);
		}

		_boardData = data;
	}

	/**
	 * Sets the players array with the types supplied, and markers (x and o)
	 * chosen at random
	 * @private
	 */
	function _setupPlayers(playerTypes) {
		var initialIndex = 0;

		if (playerTypes.length == 2) {
			initialIndex = Math.round(Math.random());

			_players = [
				{
					marker: _playerMarks[initialIndex],
					type: playerTypes[0]
				},
				{
					marker: _playerMarks[(!initialIndex | 0)],
					type: playerTypes[1]
				}
			]
		}
	}

	/**
	 * Rotates the players. Switches which player is currently active
	 * @private
	 */
	function _rotatePlayers() {
		_players.push( _players.shift() );
	}

	/**
	 * Delegate getting the player's position determined by their type
	 * @private
	 */
	function _getPlayerMove() {
		var pos = [];

		if (_players[0].type == 'ai') {
			pos = _aiMove(_addPlayerToBoard);
		} else {
			pos = _getInput(_addPlayerToBoard);
		}
	}

	/**
	 * Get the AI's position by choosing a random empty cell
	 * @private
	 */
	function _aiMove(callback) {
		var index = -1
			, pos = [];

		// Choose a random index from the available positions
		index = Math.floor(Math.random() * _boardData.empty.length - 0);

		pos = _posFromString(_boardData.empty[index]);

		setTimeout(function() {
			callback(pos);
		}, 1000);
	}

	/**
	 * Get the human's position from the user input
	 * @private
	 */
	function _getInput(callback) {
		_rl.question('Player ' +  _players[0].marker + '\'s move (enter row column): ', function(input) {

			// Ensure input is an available cell
			if (_boardData.empty.indexOf(input) != -1) {
				callback(_posFromString(input));
			} else {
				_getInput(callback);
			}
		});
	}

	/**
	 * Parse a space separate string of '[row] [column]' as a board position
	 * @private
	 */
	function _posFromString(str) {
		var pos = str.split(' ');

		pos.forEach(function(el, i, arr) {
			arr[i] = parseInt(el);
		});

		return pos;
	}

	/**
	 * Add the active player to the board, print the board, and check
	 * for a winner. If no winner is found, start again.
	 * @private
	 * @param {Array} The player's position
	 */
	function _addPlayerToBoard(pos) {
		_addToBoard(_players[0], pos);

		console.log('\u001b[2J\u001b[0;0H');
		_printBoard();

		if (_boardData.empty.length == 0) {
			console.log('Draw!');
			_rl.close();
			return;
		}

		if (_checkBoard(_players[0])) {
			_rl.close();
			return;
		} else {
			_rotatePlayers();
			_getPlayerMove(_players[0]);
		}
	}

	/**
	 * Mark the board with the player's position, and remove that cell
	 * from the array of empty cells.
	 * @private
	 * @param {Object} The player to add
	 * @param {Array} The player's position
	 */
	function _addToBoard(player, pos) {
		var emptyIndex = -1;

		// Add the player's position to the board
		_boardData.board[pos[0]][pos[1]] = player.marker;

		// Remove this position from the array of empty cells
		emptyIndex = _boardData.empty.indexOf(pos[0] + ' ' + pos[1]);
		_boardData.empty.splice(emptyIndex, 1);
	}

	/**
	 * Print the board
	 * @private
	 */
	function _printBoard() {
		var divider = '+-----------+';

		for (var i = 0; i < 3; i += 1) {
			console.log(divider);
			var row = '|';

			for (var k = 0; k < 3; k += 1) {
				if (_boardData.board[i][k])
					row += ' ' + _boardData.board[i][k] + ' |';
				else
					row += '   |';
			}

			console.log(row);
		}

		console.log(divider);
	}

	/**
	 * Check the board to see if the player had a winning move
	 * @private
	 * @param {Object} The player
	 */
	function _checkBoard(player) {
		var board = _boardData.board
			, playerStr = player.marker + player.marker + player.marker
			, winner = null
			, colStrs = ['', '', '']
			, diagStrs = ['', ''];

		for (var i = 0; i < 3; i += 1) {

			// Check each row for a winner
			var rowStr = board[i].join('');

			if (rowStr == playerStr) {
				winner = player;
				break;
			}

			// Build a string of column values
			for (var k = 0; k < 3; k += 1) {
				colStrs[k] += board[i][k];
			}

			// Build a string of diagonal values
			if (i == 0) {
				diagStrs[0] += board[i][0];
				diagStrs[1] += board[i][2];
			} else if (i == 1) {
				diagStrs[0] += board[i][1];
				diagStrs[1] += board[i][1];
			} else if (i == 2) {
				diagStrs[0] += board[i][2];
				diagStrs[1] += board[i][0];
			}
		}

		// Check the column strings for a winner
		if (colStrs.indexOf(playerStr) > -1) {
			winner = player;
		}

		// Check the diagonals for a winner
		if (diagStrs.indexOf(playerStr) > -1) {
			winner = player;
		}

		if (winner) {
			console.log(winner.marker + ' wins!');
			return true;
		}

		return false;

	}


	/*
	 * Public API
	 * ------------------------------------------------------------------------
	 */

	return {

		/**
		 * Start the game
		 * @param {Array} A two element array of player types. Possible values
		 * are 'human' or 'ai'.
		 */
		start: function(playerTypes) {
			_buildBoard();
			_setupPlayers(playerTypes);

			if (_players[0].marker != 'x') {
				// Rotate the players so that 'x' goes first
				_rotatePlayers();
			}

			_getPlayerMove();
		},

		/**
		 * Return the array of players
		 */
		getPlayers: function() {
			return _players;
		}

	}
};

ticTacToe = new TicTacToe(readline)
ticTacToe.start(['human', 'ai']);

/*const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Guess The Number Between 1 to 10 ?', (answer) => {
  // TODO: Log the answer in a database
  console.log(`Thank you for your valuable feedback: ${answer}`);

  rl.close();
});
/*const { once } = require('events');
const { createReadStream } = require('fs');
const { createInterface } = require('readline');

(async function processLineByLine() {
  try {
    const rl = createInterface({
      input: createReadStream('sample.txt'),
      crlfDelay: Infinity
    });

    rl.on('line', (line) => {
      // Process the line.
    });

    await once(rl, 'close');

    console.log('File processed.');
  } catch (err) {
    console.error(err);
  }
})();

const fs = require('fs');
const readline = require('readline');

const rl = readline.createInterface({
  input: fs.createReadStream('sample.txt'),
  crlfDelay: Infinity
});

rl.on('line', (line) => {
  console.log(`Line from file: ${line}`);
});
const fs = require('fs');
const readline = require('readline');

async function processLineByLine() {
  const fileStream = fs.createReadStream('sample.txt');

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in input.txt as a single line break.

  for await (const line of rl) {
    // Each line in input.txt will be successively available here as `line`.
    console.log(`Line from file: ${line}`);
  }
}

processLineByLine();

const readline = require('readline');
const r1 = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: 'Enter the Number>'
});
r1.prompt();
r1.on('line', (line) => {
  switch (line) {
    case line:
    if(line==9){
      console.log(`This is correct Number'${line}'`);
    }
    else if(line !=9){
     console.log(`This is Not Correct Number'${line}'`)
    }
      break;
    default:
      console.log(`Please Enter The Number`);
      break;
  }
  r1.prompt();
})
.on('close',()=>{
  process.exit(0);
})
/*.on('close', () => {
  console.log('Have a great day!');
  process.exit(0);
});
/*const choose2=(input,array)=>{
var prop=Object.keys(input);
for(var i=0;i<array.length;i++){
var str=array[i].split('.');
for(var j=0;j<str.length;j++){
if(input[str[j]]=='Object'){
  str[j]={};
}
}

}

}



let input = {
    name: {
        first: 'John',
        last: 'Cena',
        nick: 'cena'
    },
    age: {years: 23, months: 5, days: 2},
    sex: 'M',
    matches: [
        {day: 0, session: 'morning', with: {name: 'Jeff', age: {years: 20, months: 10, days: 2}}},
        {day: 1, session: 'noon', with: {name: 'Jeff', age: {years: 21, months: 8, days: 3}}},
        {day: 1, session: 'evening', with: {name: 'Jeff', age: {years: 24, months: 5, days: 4}}},
    ]
};

console.log(choose2(input, [
    'name.first',
    'name.last'
  ]));
    /*'age.years',
    'matches.0.session',
    'matches.0.with.name',
    'matches.0.with.age.years',
    'matches.0.with.age.months',
    'matches.1.session',
    'matches.1.with.age.years',
    'matches.1.with.age.months',
    'matches.1.with.age.days',
]))//.to.eql({
    name: {first: 'John', last: 'Cena'},
    age: {years: 23},
    matches: [
        {session: 'morning', with: {name: 'Jeff', age: {years: 20, months: 10}}},
        {session: 'noon', with: {age: {years: 21, months: 8, days: 3}}}
    ]
});
});

/*function Game() {
  var result = [];
  return {
    moves: function(arr) {
      result[0] = ['-', '-', '-'];
      result[1] = ['-', '-', '-'];
      result[2] = ['-', '-', '-'];
      for (var i = 0; i < arr.length; i++) {
        result[arr[i][1]][arr[i][0]] = arr[i][2];
      }
    },
    getState: function() {
      var exp = result;
      var str;
      for (var i = 0; i < exp.length; i++) {
        if (str != undefined) {
          str += '\n' + exp[i].join('');
        } else {
          str = exp[i].join('');
        }
      }
      return str;
    },
    load: function(string) {
      var arr1 = [];
      var str1 = string.replace(/\s+/g, '\n').split('\n');
      console.log(str1);
      for (var i = 0; i < str1.length; i++) {
        arr1[i] = []
        arr1[i][0] = str1[i][0]
        arr1[i][1] = str1[i][1]
        arr1[i][2] = str1[i][2]
        arr1[i][3] = str1[i][3]
      }
      return result = arr1;
    },
    getWinner: function() {
      var win = result;
      var winner;
      for (var i = 0; i < win.length; i++) {
        if ((win[i][0] == 'A') && (win[i][1] == 'A') && (win[i][2] == 'A')) {
          return winner = 'A';
        }
        if ((win[i][0] == 'B') && (win[i][1] == 'B') && (win[i][2] == 'B')) {
          return winner = 'B';
        }
        if ((win[0][i] == 'A') && (win[1][i] == 'A') && (win[2][i] == 'A')) {
          return winner = 'A';
        }
        if ((win[0][i] == 'B') && (win[1][i] == 'B') && (win[2][i] == 'B')) {
          return winner = 'B';
        }
      }
      for (var j = 0; j < win.length; j++) {
        if (win[j][j] == 'A') {
          return winner = 'A';
        }
        if (win[j][j] == 'B') {
          return winner = 'B'
        }
        if (win[2 - j][j] == 'A') {
          return winner = 'A';
        }
        if (win[2 - j][j] == 'B') {
          return winner = 'B'
        }
      }
    }
  }
}
let game1 = new Game();
console.log(game1.load(
  `AAB-
    -B--
    -B--
    AAAA`
));
console.log(game1.getWinner());
const expected =
  `--B
 ABA
 BA-`.replace(/\s+/g, '\n');
console.log(game1.getState());


/*const raw = {
  item1: { key: 'sdfd', value:'sdfd' },
  item2: { key: 'sdfd', value:'sdfd' },
  item3: { key: 'sdfd', value:'sdfd' }
};

const allowed = ['item1', 'item3'];
const filtered = Object.keys(raw)
  .filter(key =>allowed.includes(key))
  .reduce((obj, key) => {
    obj[key] = raw[key];
    return obj;
  }, {});

console.log(filtered);
[
  [0, 1, 'A'],
  [0, 2, 'B'],
  [1, 2, 'A'],
  [1, 1, 'B'],
  [2, 1, 'A'],
  [2, 0, 'B'],
]
const choose2 = (input, fill) => {
let z;
var obj={};
let key=fill[0].split('.')
console.log(obj[key[0]][key[1]]=input[key[0]][key[1]])
}
let person = {
  'name': {
    'first': 'John',
    'last': 'Cena'
  },
  'age': 23,
  'sex': 'M'
};
//console.log(choose2(person, ['sex']));
//console.log(choose2(person, ['age']));
console.log(choose2(person, ['name.first']));



function Game() {
  var result = [];
  return {
    moves: function(arr) {
      result[0] = [];
      result[1] = [];
      result[2] = [];
      for (var i = 0; i < arr.length; i++) {
        result[arr[0][i]][arr[i][1]] = arr[i][2];
      }
    },
    getWinner: function() {
      var win = result;
      var winner;
      for (var i = 0; i < win.length; i++) {
        if ((win[i][0] == 'A') && (win[i][1] == 'A') && (win[i][2] == 'A')) {
          return winner = 'A';
        } else if ((win[i][0] == 'B') && (win[i][1] == 'B') && (win[i][2] == 'B')) {
          return winner = 'B';
        }
      }
    }
  }
}
var game = new Game();
console.log(game.moves([
  [0, 0, 'A'],
  [1, 0, 'B'],
  [0, 2, 'A'],
  [1, 1, 'B'],
  [2, 0, 'A'],
  [1, 2, 'B']
]));

console.log(game.getWinner());

/*const reduce = (array, add) => {
  var result = array[0];
  for (var i = 1; i < array.length; i++) {
    result = add(result, array[i]);
  }
  return result;
}
const isOdd = (n) => n % 2 == 1;
const filter = (input, obj) => {
  var result = [];
  for (var i = 0; i < input.length; i++) {
    if (typeof obj == 'object') {
      for (var p in input[i]) {
        if (input[i][p] == obj[p]) {
          result.push(input[i]);
          break;
        }
      }
    } else if (typeof obj == 'function') {
      if (obj(input[i]) == true) {
        result.push(input[i]);
      }
    }
  }
  return result;
}
const chain = (input, arr) => {
  var val = input;
  for (var i = 0; i < arr.length; i++) {
    val = arr[i][0](val, arr[i][1])
  }
  return val;
}





console.log(chain([1, 2, 3, 4, 5], [
  [filter, isOdd],
  [reduce, (a, b) => a + b]
]));





/*
function GameIsOver() {
  var Draw = "Match draw For X AND Y";
  var a = 'X';
  var b = 'Y';
  if ((j[0][0] == a) && (j[1][1] == a) && (j[2][2] == a)) {
    return a;
  }
  if ((j[0][0] == b) && (j[1][1] == b) && (j[2][2] == b)) {
    return b;
  }
  if ((j[2][0] == a) && (j[1][1] == a) && (j[0][2] == a)) {
    return a;
  }
  if ((j[2][0] == b) && (j[1][1] == b) && (j[0][2] == b)) {
    return b;
  }
  if ((j[0][0] == a) && (j[0][1] == a) && (j[0][2] == a)) {
    return a;
  }
  if ((j[0][0] == b) && (j[0][1] == b) && (j[0][2] == b)) {
    return b;
  }
  if ((j[1][0] == a) && (j[1][1] == a) && (j[1][2] == a)) {
    return a;
  }
  if ((j[1][0] == b) && (j[1][1] == b) && (j[1][2] == b)) {
    return b;
  }
  if ((j[2][0] == a) && (j[2][1] == a) && (j[2][2] == a)) {
    return a;
  }
  if ((j[2][0] == b) && (j[2][1] == b) && (j[2][2] == b)) {
    return b
  }
  if ((j[0][0] == a) && (j[1][0] == a) && (j[2][0] == a)) {
    return a;
  }
  if ((j[0][0] == b) && (j[1][0] == b) && (j[2][0] == b)) {
    return b;
  }
  if ((j[0][1] == a) && (j[1][1] == a) && (j[2][1] == a)) {
    return a;
  }
  if ((j[0][1] == b) && (j[1][1] == b) && (j[2][1] == b)) {
    return b;
  }
  if ((j[0][2] == a) && (j[1][2] == a) && (j[2][2] == a)) {
    return a;
  }
  if ((j[0][2] == b) && (j[1][2] == b) && (j[2][2] == b)) {
    return b;
  }
  return Draw
}
console.log(GameIsOver());


    function resolve(path, obj, separator = '.') {
      var props = Array.isArray(path) ? path : path.split(separator)
      var value = props.reduce((prev, curr) => prev && prev[curr], obj)
      var data = {},
        temp = data;
      for (var i = 0; i < props.length; i++) {
        var key = props[i];
        if (i < props.length - 1) {
          if (temp[key] === undefined) {
            temp[key] = {};
          }
        } else {
          temp[key] = value;
        }
        temp = temp[key];
      }
      return data;
    }
    let arr = [
      'name.first',
      'name.last',
      'age.years',
      'matches.0.session',
      'matches.0.with.name',
      'matches.0.with.age.years',
      'matches.0.with.age.months',
      'matches.1.session',
      'matches.1.with.age.years',
      'matches.1.with.age.months',
      'matches.1.with.age.days',
    ]
    for (var i = 0; i < arr.length; i++) {
      var value = resolve(arr[i], input);
    }
    /*var sp=arr[i].split('.');
    for (var j = 0; i < sp.length; j++) {
        var key = sp[j];
    console.log(value);
        if (j < sp.length - 1) {
            if (temp[key] === undefined) {
                temp[key] = {};
            }
        } else {
            temp[key] = value;
        }
        temp = temp[key];
    }
    }
    console.log(data);


    const choose2 = (obj, arr) => {
      var obj1 = {};
      for (var i = 0; i < arr.length; i++) {
        var keys = arr[i].split('.');
        var temp=obj[0];
        for (var j = 1; j < keys.length; j++) {
          temp=temp[j];
          if (j < keys.length - 1) {
            if (temp[key] === undefined) {
              temp[key] = {};
            }
          } else {
            temp[key] = value;
          }

        }
      }
    }


    let actual=choose2(input, [
        'name.first']);
    /*    'name.last',
        'age.years',
        'matches.0.session',
        'matches.0.with.name',
        'matches.0.with.age.years',
        'matches.0.with.age.months',
        'matches.1.session',
        'matches.1.with.age.years',
        'matches.1.with.age.months',
        'matches.1.with.age.days',
    ]);




    /*.to.eql({
        name: {first: 'John', last: 'Cena'},
        age: {years: 23},
        matches: [
            {session: 'morning', with: {name: 'Jeff', age: {years: 20, months: 10}}},
            {session: 'noon', with: {age: {years: 21, months: 8, days: 3}}}
        ]
    });
    });
    // Now we have: obj.shapes.rectangle.height === 400


    // expected output: 42

    //console.log(new Food('cheese', 5).price);
    // expected output: "cheese"






    /*function Archiver() {
      var temperature = null;
      Object.defineProperty(this, 'temperature', {
        get: function() {
          console.log('get!');
          return temperature;
        }
      });
    }

    var arc = new Archiver();
    console.log(arc);
    arc.temperature; // 'get!'
    arc.temperature = 30;
    console.log(arc.temperature);

    const proxy2 = () => {
      var count = 0;
      var increment = function() {
          return ++count;
      }
      return {
        get:count
      };
    }
    const obj=proxy2();
    console.log(obj())
    console.log(obj.get)
    console.log(obj.reset)








    /*original = [];
    obj = Js.proxy3(original);
    expect(obj).to.equal(original);

    obj.val = 100;
    expect(obj.get).to.eql(200);
    obj.reset;

    expect(obj.get).to.eql(undefined);







    /*
    var obj = {counter:0}

    // Define object

    // Define setters
    Object.defineProperty(obj, "reset", {
      get : function () {this.counter = 0;}
    });
    Object.defineProperty(obj, "increment", {
      get : function () {this.counter++;}
    });
    Object.defineProperty(obj, "decrement", {
      get : function () {this.counter--;}
    });
    Object.defineProperty(obj, "add", {
      set : function (value) {this.counter += value;}
    });
    Object.defineProperty(obj, "subtract", {
      set : function (value) {this.counter -= value;}
    });
    console.log(obj.reset);
    obj.add = 5;
    obj.subtract = 1;
    obj.increment;
    obj.decrement;
    console.log(obj.counter);
    console.log(obj.reset);
    console.log(obj.counter);

    /*const prof=()=>{
      return{
        get fullName(){
          return fullName*2;
        },
        set fullName(x){
            fullName=x;
        }
    }
    }
    var obj=prof();
    obj.fullName=100
    console.log(obj.fullName);
    //obj.fullName = 'Jack Franklin';
    //console.log(obj.reset);
    //console.log(obj.add = 5);
    //console.log(obj.subtract = 1) ;
    //console.log(obj.increment);
    //console.log(obj.decrement);
    //console.log(obj.counter)

    //console.log(o.a); // 7
    //console.log(o.b); // 8
    /*function Book (type, author) {
        this.type = type;
        this.author = author;
        this.getDetails = function () {
            return this.type + " written by " + this.author;
        }
    }
    var book =
    console.log(book.getDetails());






    /*const counter3 = () => {
      var x = 0;
      return new Function(get,reset)(
        x+=1
    return function get(){return x};)

    }
    //counter.reset;
    console.log(counter.get);//.to.eql(0);
    counter();
    /*expect(counter.get).to.eql(1);
    counter();
    expect(counter.get).to.eql(2);
    counter();
    expect(counter.get).to.eql(3);
    counter();
    expect(counter.get).to.eql(4);
    counter.reset;
    expect(counter.get).to.eql(0);
    });

















    /*actual.inc()
    console.log(actual.get());
    actual.reset();
    console.log(actual.get());

    //counter.inc();
    //console.log(counter.get)//.to.eql(1);
    //counter.inc();
    /*console.log(counter.get())//.to.eql(2);
    counter.inc();
    console.log(counter.get())//.to.eql(3);
    counter.inc();
    console.log(counter.get())//.to.eql(4);


    /*
    const counter = Js.counter3();

    counter.reset;
    expect(counter.get)//.to.eql(0);
    counter();
    expect(counter.get)//.to.eql(1);
    counter();
    expect(counter.get)//.to.eql(2);
    counter();
    expect(counter.get)//.to.eql(3);
    counter();
    expect(counter.get).to.eql(4);
    counter.reset;



    //counter1(2);
    //console.log(counter1()); /* Alerts 2 */
/*counter1.decrement();
alert(counter1.value()); /* Alerts 1 */
/*alert(counter2.value()); /* Alerts 0 */

/*const textacc=()=>{
var x='';
    return function acc1(str){
      x +=str;
    }
  }
const acc1 = textacc();

//console.log(acc1());//.to.eql('');

acc1(' hi ');
//console.log(acc1())//.to.eql(' hi ');
//console.log(acc1());//.to.eql('');
/*
acc1(' hi ');
acc1(' hello ');
acc1(' how are you? ');
console.log(acc1());//.to.eql(' hi \n hello \n how are you? ');
consoel.log(acc1());//.to.eql('');

acc1('');
acc1('');
console.log(acc1());//.to.eql('\n');
console.log(acc1())//.to.eql('');


/*function makeFunc() {
  var name = 'Mozilla';
  function displayName() {
return name;
  }
  return displayName;
}

var myFunc = makeFunc();
console.log(myFunc());




// QUESTION:
//what is the constructor?
//toString
//global and local variable
//inner function fnName

//
//
//
//
//
//
//
//
// /*
// const godFunction2 = (name, var1Name, var2Name, body) => {
//   const bodyWrapper = 'const ' + name + ' = (' + var1Name + ',' + var2Name + ') => { return ' + body + '; }; return ' + name + ';'
//   console.log(bodyWrapper);
//   // const add = () => { return a+b; }; return add;
//
//   return new Function(bodyWrapper)();
// }
//
// const j = godFunction2('add', 'a', 'b', 'a+b');
// console.log(j(1,12), j.name);
// */
// const ki = 300;
//
// const kilo = () => {
//   const j = 100;
//   console.log(j);
//   console.log(typeof gg);
//   console.log(typeof ki);
//   console.log(ki);
// }
//
// const milli = () => {
//   const gg = 200;
//   console.log(gg);
//   console.log(typeof j);
//   console.log(typeof ki);
//   console.log(ki);
// }
//
// const lala = () => {
//   const jij = () => {
//     const gu = 500;
//     console.log(gu);
//
//     console.log(typeof hu);
//   }
//
//   const juju = () => {
//     const hu = 600;
//     console.log(hu);
//
//     console.log(typeof gu);
//   }
//
//   jij();
//   juju();
// }
//
// // kilo();
// // milli();
// lala();
//
// const morningDuties =()=>{}
// const lunchDuties = ()=>{}
// const dinnerDuties = ()=>{}
// const dailyDuties = ()=>{
//   morningDuties();
//   lunchDuties();
//   dinnerDuties();
// };
//
// const weeklyDuties = ()=>{
//   dailyDuties(); //monday
//   dailyDuties(); //monday
//   dailyDuties(); //monday
//   dailyDuties(); //monday
//   dailyDuties(); //monday
//   holidayDuties();
//   holidayDuties();
// }
//
// const weeklyDutieson1stSat = ()=>{
//   dailyDuties(); //monday
//   dailyDuties(); //monday
//   dailyDuties(); //monday
//   dailyDuties(); //monday
//   dailyDuties(); //monday
//   dailyDuties(); //monday
//   holidayDuties();
// }
//
// const monthlyDuties = ()=>{
//   weeklyDutieson1stSat();
//   weeklyDuties();
//   weeklyDuties();
//   weeklyDuties();
// }
//
//
// const add = (a,b)=> a+b;
// const incBy5 = (b) => add(5,b);
//
// add(1,2); //3
// incBy5(10); //15
//node js readline api
