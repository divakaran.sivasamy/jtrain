# jtrain

### dependencies
npm install

### get new tests
```
git fetch upstream
git merge upstream/master
```

### run tests
```
#Run all tests
npm test

#Run only fns tests
npm run fns

#Run only tictactoe tests
npm run tictactoe
```

### submit code
git push
