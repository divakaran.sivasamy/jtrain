const readline = require('readline');
const number = Math.ceil(10 * Math.random())
let times = 5;
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: 'Enter The Number Between 1 To 10 >'
});
rl.prompt();
rl.on('line', (answer) => {
  if ((parseInt(answer) != NaN) && (parseInt(answer) < 11)) {
    if (parseInt(answer) == number) {
      console.log(`Wow!,You Got the Correct Number:${number}`);
      rl.close();
      process.stdin.destroy();
    }
    times--;
    if ((parseInt(answer) != number) && (times != 0)) {
      console.log(`Sorry!,You Enter The Number is Wrong Guess again`);
      rl.prompt();
    } else if ((parseInt(answer) != number) && (times == 0)) {
      console.log('Sorry!,You Enter The Number is Wrong');
      console.log(`The Correct Number is : ${number}`);
      console.log('Game Is Over');
      process.exit(0);
    }
  } else {
    console.log('Please Enter The Correct Value');
    rl.prompt();
  }

});
