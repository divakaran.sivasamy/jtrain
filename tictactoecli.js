const readline = require('readline');
const T2 = require('./tictactoe.js');
const c = require('ansi-colors');

function ticTacToe(readline) {
  var arr1 = [];
  var pos = [];
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  const game = new T2.Game();
  console.log('\u001b[2J\u001b[0;0H');
  console.log(game.getState());

  function player(name) {
    rl.question('Player  ' + name + '  moves (enter row column): ', function(input) {
      pos = input.split(' ');
      pos.forEach(function(el, i, arr) {
        arr[i] = parseInt(el);
      })
      if (game.check(pos) != undefined) {
        pos.push(name);
        arr1.push(pos);
        game.moves(arr1);
        console.log('\u001b[2J\u001b[0;0H');
        console.log(game.getState());
        if (game.getWinner() != undefined) {
          console.log(c.green('The Winner is ' + game.getWinner() + ' !'));
          rl.question('Are you want to Play Again(y or n)', function(option) {
            if (option == 'y') {
              rl.close();
              ticTacToe(readline);
            } else {
              console.log('Bye Have a Nice Day');
              rl.close();
            }
          })
        } else {
          if (name == 'A') {
            player('B');
          } else {
            player('A');
          }
        }
      } else {
        if (isNaN(pos[0]) || isNaN(pos[1])) {
          console.log('\u001b[2J\u001b[0;0H');
          console.log('Does NOt Allowed  ' + c.red(input) + ' characters value')
          console.log('Please Enter Number value');
          console.log(' ');
          console.log(game.getState())
          player(name);
        } else if (pos.length != 2) {
          console.log('\u001b[2J\u001b[0;0H');
          console.log('Dont use this Format  ' + c.red(input) + ' value')
          console.log('Please Enter Correct Format,  ex: 0<space>0');
          console.log(' ');
          console.log(game.getState())
          player(name);
        } else {
          if (name != 'A') {
            console.log('\u001b[2J\u001b[0;0H');
            console.log('Field ' + c.red(input) + ' Already Used!. Please Select Different Field');
            console.log(' ');
            console.log(game.getState());
            player('B');
          } else {
            console.log('\u001b[2J\u001b[0;0H');
            console.log('Field ' + c.red(input) + ' Already Used!. Please Select Different Field');
            console.log(' ');
            console.log(game.getState());
            player('A');
          }
        }
      }
    });

  }
  player('A');
}
ticTacToe(readline);
//
