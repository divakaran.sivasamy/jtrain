var game = new Game();
const gameEN = document.getElementById("game");
const GAME_SIZE = 3;

for (var y = 0; y < GAME_SIZE; y++) {
  const tr = document.createElement('tr');
  for (var x = 0; x < GAME_SIZE; x++) {
    const td = document.createElement('td');
    td.setAttribute('class', 'td');
    td.setAttribute('id', x + ',' + y);
    td.setAttribute('onclick', 'handleMove(' + x + ',' + y + ')');
    tr.appendChild(td);
  }
  gameEN.appendChild(tr);

}
var playerTurn = document.getElementById("player");
playerTurn.innerHTML = "player A turn's";

function handleMove(x, y) {
  const chooseEle = document.getElementById(x + ',' + y);
  if (game.isEmpty(x, y)) {
    game.moves([
      [x, y]
    ]);
    updateBoard(chooseEle);
  } else {
    alert('This Cell  ' + x + ',' + y + '  Already Used Please select different cell');
  }
}

function updateBoard(element) {
  const board = game.getBoard();
  var play = game.playCount();
  for (var i = 0; i < board.length; i++) {
    for (var j = 0; j < board.length; j++) {
      if (element.id == (i + ',' + j)) {
        element.innerHTML = board[i][j];
        playerTurn.innerHTML = "player " + board[i][j] + " turn's"
        if (game.getWinner() == board[i][j]) {
          alert('player  ' + board[i][j] + '   Has Won The Match');
          playerTurn.innerHTML = "ReStart New Game";
          playerTurn.setAttribute("onClick", 'refresh()');
        } else if (play[board[i][j]] == 5) {
          alert('Match Draw');
          playerTurn.innerHTML = "ReStart New Game";
          playerTurn.setAttribute("onClick", 'refresh()');
        } else {
          var turner = board[i][j];
          turner = turner === "A" ? "B" : "A"
          playerTurn.innerHTML = "player " + turner + " turn's";

        }
      }
    }
  }
};
function refresh() {
  window.location.reload();
}
// iterate through board
// i,j (y,x) also, content of the cell (A|B|-)
// get element by id x and y
// element inner html = content
//
