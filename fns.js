/*
const fn100 = (array, addmul) => {
  var result = [];
  for (var i = 0; i < array.length; i++) {
    result.push(addmul(array[i]));
  }
  return result;
}

const fn200 = (array, asdsorder) => {
  var adoutput = [];
  var order;
  var temp;
  for (var i = 0; i < array.length; i++) {
    order = i;
    for (var j = i + 1; j < array.length; j++) {
      var adorder = asdsorder(array[j], array[order]);
      if (adorder == 1) {
        order = order;
      } else {
        order = j;
      }
    }
    temp = array[i];
    array[i] = array[order];
    array[order] = temp;
    adoutput.push(array[i]);
  }

  return adoutput;
}

*/
const each = (x, y) => {
  return x.forEach(y);
}

const map = (array, x) => {
  var result = [];
  for (var i = 0; i < array.length; i++) {
    result.push(x(array[i]));
  }
  return result;
}

const sortBy = (array, asdsorder) => {
  var output = [];
  var order;
  var temp;
  for (var i = 0; i < array.length; i++) {
    order = i;
    for (var j = i + 1; j < array.length; j++) {
      var adorder = asdsorder(array[order], array[j]);
      if (adorder < 0) {
        order = order;
      } else {
        order = j;
      }
    }
    temp = array[i];
    array[i] = array[order];
    array[order] = temp;
    output.push(array[i]);
  }
  return output;
}

const keys = (input) => {
  var result = Object.keys(input);
  return result;
}

const some = (array, oddnum) => {
  var result = array.filter(oddnum);
  if (typeof result[0] == 'number') {
    return true;
  } else if (typeof result[0] == 'object') {
    return true;

  }
  return false;
}

const every = (array, evenodd) => {
  var result = array.filter(evenodd);
  if (array.length == result.length) {
    return true;
  }
  return false;
}

const atleast2 = (array, odd2) => {
  var resul = [];
  for (var i = 0; i < array.length; i++) {
    if (true == odd2(array[i])) {
      resul.push(array[i]);
    }
  }
  if (2 == resul.length) {
    var tf = true;
  } else {
    tf = false
  }
  return tf;
}

const everybut1 = (array, odd3) => {
  var result1 = [];
  for (var i = 0; i < array.length; i++) {
    if (true != odd3(array[i])) {
      result1.push(array[i]);
    }
  }
  if (1 == result1.length) {
    var t = true;
  } else {
    t = false;
  }
  return t;
}

const padding = (x, y) => {
  var pad = ' ';
  var sd = '';
  for (var i = 0; i < y - x.length; i++) {
    sd = sd + pad;
  }
  return sd + x;
}

const choose2 = (input, array) => {
  var object = {};
  for (var i = 0; i < array.length; i++) {
    var str = array[i].split('.');
    if (str.length != 1) {
      for (var j = 0; j < str.length; j++) {
        var obj = {};
        var temp = input[str[j]];
        if (typeof temp == 'object') {
          var arr = str[j];
        }
        obj[str[j]] = temp;
      }
      object[array[i]] = obj;
    } else if (str.length == 1) {
      object[array[i]] = input[array[i]];
    }
  }
  return object;
}

const choose1 = (input, x) => {
  var str = x.split('.');
  for (var i = 0; i < str.length; i++) {
    var temp = input[str[i]];
    var input = temp;
  }
  return temp;
}

const intersection = (array1, array2) => {
  var alen = array1.length;
  var blen = array2.length;
  var inter = [];
  for (var i = 0; i < alen; i++) {
    for (var j = 0; j < blen; j++) {
      if (array1[i] == array2[j]) {
        inter.push(array1[i]);
      }
    }
  }
  return inter;
}
const intersectionBy = (arr1, arr2, func) => {
  var result = [];
  for (var i = 0; i < arr1.length; i++) {
    for (var j = 0; j < arr2.length; j++) {
      if (func(arr1[i]) == func(arr2[j])) {
        result.push(arr1[i]);
      }
    }
  }
  return result;
}

const zip = (...arrays) => {
  var output = [];
  for (var i = 0; i < arrays.length; i++) {
    var row = arrays[i];
    row.length = arrays[0].length;
    for (var j = 0; j < row.length; j++) {
      output[j] = output[j] || [];
      output[j].push(row[j]);
    }
  }
  return output;
}

const reverse = (array) => {
  var result = [];
  for (var i = array.length - 1; i >= 0; i--) {
    result.push(array[i]);
  }
  return result;
}

const filter = (input, obj) => {
  var result = [];
  for (var i = 0; i < input.length; i++) {
    if (typeof obj == 'object') {
      for (var p in input[i]) {
        if (input[i][p] == obj[p]) {
          result.push(input[i]);
          break;
        }
      }
    } else if (typeof obj == 'function') {
      if (obj(input[i]) == true) {
        result.push(input[i]);
      }
    }
  }
  return result;
}



const merge = (ob1, ob2, ob3) => {
  var ob1 = Object.assign(ob1, ob2, ob3);
  return ob1;
}
const mergeDeep = (obj1, obj2) => {
  var obj = {};
  var key = Object.keys(obj1);
  for (var i = 0; i < key.length; i++) {
    if (obj1[key[i]] == obj2[key[i]]) {
      obj[key[i]] = obj1[key[i]];
    } else {
      obj[key[i]] = Object.assign(obj1[key[i]], obj2[key[i]]);
    }
  }
  return obj;
}

const superfunction1 = () => {
  return () => 100;
}

const superfunction2 = (x) => {
  return (...fn) => x;
}

const superfunction3 = (x) => {

  return (...fn) => x();
}

const superfunction4 = (x) => {
  return function() {
    return x += 1;
  }
}


const superfunction5 = (x) => {
  var i = -1;
  return function() {
    i++
    return x[i];
  }
}


const godFunction = (arr, exp) => {
  return function(x, y, z) {
    var a = x;
    var b = y;
    var c = z;
    return eval(exp);
  }
}

const godFunction2 = (name, arr, exp) => {
  const body = 'const ' + name + '= (' + arr[0] + ',' + arr[1] + ')=>{return ' + exp + ';}; return ' + name + ';'
  return new Function(body)();
}


const split = (mystring, separator) => {
  var splitstr = mystring.split([separator]);
  return splitstr;
}


const before = (x, y) => {
  for (var i = 0; typeof x == 'function'; i++) {
    x = x();
  }
  for (var j = 0; typeof y == 'function'; j++) {
    y = y();
  }
  return x < y;
}


const object1 = (x, y) => {
  var object = {};
  object.name = x;
  object.age = y;
  return object;
}
const object2 = (x, y) => {
  var object = {};
  object.getName = () => x;
  object.getAge = () => y;
  return object;
}

class Person {
  constructor(x, y, z) {
    this.getname = x;
    this.getage = y;
    this.getsex = z;
  }
  getName() {
    return this.getname;
  }
  getAge() {
    return this.getage;
  }
  getSex() {
    return this.getsex;
  }
}

class Woman extends Person {
  constructor(x, y, z) {
    super(x, y, z);
    var z = 'F';
    this.getname = x;
    this.getage = y;
    this.getsex = z;
  }
}

class Man extends Person {
  constructor(x, y, z) {
    super(x, y, z);
    var z = 'M';
    this.getname = x;
    this.getage = y;
    this.getsex = z;
  }
}

const uniquee = (array) => {
  //var unique = array.filter((item, index) => array.indexOf(item) === index);
  var unique = array.reduce(function(acc, cur) {
    if (acc.indexOf(cur) === -1) {
      acc.push(cur);
    }
    return acc;
  }, []);
  //  unique = [...new Set(array)];
  return unique;

}
const uniqueeBy = (arr, condition) => {
  let uniqueby = arr.map(obj => condition(obj))
    .map((obj, i, final) => final.indexOf(obj) === i && i)
    .filter(obj => arr[obj]).map(obj => arr[obj]);
  return uniqueby;
}

const first = (array) => {
  for (var i = 0; i < array.length; i++) {
    if (i == 0) {
      var obj = array[i];
    }
  }
  return obj;
}


const last = (array) => {
  for (var i = 0; i < array.length; i++) {
    if (i == array.length - 1) {
      var obj1 = array[i];
    }
  }
  return obj1;
}

const reuse1 = (array, odd) => {
  var result = [];
  array.map(obj => {
    var key = Object.keys(obj);
    if (odd(obj[key]) == true) {
      result.push(obj[key]);
    }
  });
  return result;
}


const reuse2 = (array, string, odd1) => {
  var result = [];
  array.map(obj => {
    var propertyName = string;
    if (odd1(obj[propertyName])) {
      result.push(obj[propertyName])
    }
  });
  return result;
}
const reuse3 = (array, prop, odd2) => {
  //return reduce(filter(map(array, (element)=> choose1(element, props.join('.'))),fn), (a+b)=> a+b);
  //}
  var result = [];
  array.map(o => {
    var check = prop.reduce(function(o, key) {
      return o[key];
    }, o)
    if (odd2(check) == true) {
      result.push(check);
    }
  });
  return result;
}

const reuse4 = (array, prop, odd3) => {
  return array.filter(o => {
    var result = prop.reduce(function(o, key) {
      return o[key];
    }, o)
    if (odd3(result) == true) {
      return result;
    }
  });
}
const reduce = (array, add) => {
  var result = array[0];
  for (var i = 1; i < array.length; i++) {
    result = add(result, array[i]);
  }
  return result;
}

const chain = (input, func) => {
  /*  var arr = input;
    let arr1 = func.map(ele => {
      return ele.reduce(function(acc, cur) {
        var val = acc(arr, cur);
        arr = val;
        return val;
      })
    })
    let value = arr1.reduce(function(ac, cu) {
      return ac = cu;
    });

    return value;
  */
  var val = input;
  for (var i = 0; i < func.length; i++) {
    val = func[i][0](val, func[i][1]);
  }
  return val;
}



const anarray = [];

const anumber = 100;

const afunction = () => {};


const seek = (string) => {
  var str = string;
  var str1 = str[0];
  for (var i = 1; i < str.length; i++) {
    if (str1 != str[i]) {
      var result = str[i];
      break;
    }
  }
  return result;
}


const AND = (x, y) => {

  return x && y;

}


const OR = (x, y) => {

  return x || y;

}

const NAND = (x, y) => {
  if (x && y) {
    return false;
  }
  return true;
}

const NOT = (x) => {
  if (x == true) {
    return false;
  }
  return true;
}

const NOR = (x, y) => {

  if (x || y) {
    return false;
  }
  return true;
}

const XOR = (x, y) => {
  if (x == y) {
    return false;
  }
  return true;
}

const XNOR = (x, y) => {
  if (x == y) {
    return true;
  }
  return false;
}

function* range(x, y, z) {
  if (z == undefined) {
    for (var i = 0; i < y; i++) {
      yield i;
    }
  }
  for (var j = 0; j < y / z; j++) {
    yield;
  }
}

class Counter {
  constructor() {
    this.value = 0;
  }
  get() {
    return this.value;
  }
  inc() {
    return this.value++;
  }
}

const counter = () => {
  var value = 0;

  function change(val) {
    value += val;
  }
  return {
    inc: function() {
      change(1);
    },
    get: function() {
      return value;
    }
  }
}
/*
const textacc = () => {
  let str = [];
  const text = function(txt) {
    if (txt == undefined) {
      const value = str.join('\n');
      str = [];
      return value;
      // accum.push(str.join('\n'));
      // return accum.reduce(function(acc, cur) {
        // return acc = cur;
      // }, joint = '', str = []);
    }
    str.push(txt);
  };
  return text;
}
*/
const textacc = () => {
  let str;
  return (text) => {
    if (text != undefined) {
      if (str != undefined) {
        str += '\n' + text;
        return;
      }
      str = text;
    }
    if (text == undefined) {
      if (str != undefined) {
        const value = str;
        str = undefined;
        return value;
      }
    }
    return '';
  };
}

const proxy1 = () => {
  let inner = {};
  var y;
  return Object.defineProperty(inner, 'val', {
    set: (x) => y = x * 2,
    get: () => y
  })
}

const proxy2 = () => {
  let inner = {};
  var y;
  return Object.defineProperty(inner, 'val', {
      set: (x) => y = x * 2
    }),
    Object.defineProperty(inner, 'get', {
      get: () => y
    }),
    Object.defineProperty(inner, 'reset', {
      get: () => y = undefined
    })
}

const proxy3 = (type) => {
  Object.defineProperty(type, 'val', {
    set: function(x) {
      this.get = x * 2;
    }
  })
  Object.defineProperty(type, 'reset', {
    get: function() {
      this.get = undefined;
    }
  })
  return type;
}

const counter3 = () => {
  let val = 0;
  const x = () => val++;
  Object.defineProperty(x, 'get', {
    get: () => val
  });
  Object.defineProperty(x, 'reset', {
    get: () => val = 0
  });
  return x;
}


const saftynet = (fnc) => {
  try {
    fnc();
  } catch (e) {
    return e;
  }
}





module.exports = {
  each,
  map,
  sortBy,
  some,
  every,
  atleast2,
  everybut1,
  padding,
  choose2,
  choose1,
  intersection,
  intersectionBy,
  keys,
  reverse,
  filter,
  merge,
  mergeDeep,
  superfunction1,
  superfunction2,
  superfunction3,
  superfunction4,
  superfunction5,
  godFunction,
  godFunction2,
  split,
  before,
  object1,
  object2,
  Person,
  Woman,
  Man,
  uniquee,
  uniqueeBy,
  first,
  last,
  reuse1,
  reuse2,
  reuse3,
  reuse4,
  chain,
  reduce,
  anarray,
  anumber,
  afunction,
  seek,
  Counter,
  counter,
  textacc,
  counter3,
  proxy1,
  proxy2,
  proxy3,
  saftynet,
  AND,
  OR,
  NAND,
  NOT,
  NOR,
  XOR,
  XNOR,
  range,
  zip
};
module.exports.me = module.exports;
/*
the following are same

const k = (a,b,c) => a+b+c;

const k = (a,b,c) => {
  return a+b+c;
};

const y = (a) => {
  return a + 1;
};

const y = (a) => a + 1;

const y = a => a + 1;
*/
